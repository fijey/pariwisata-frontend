import React, { Component } from 'react';
import DestinationServices from '../services/DestinationServices';

class CreateDestinationsComponent extends Component {

    constructor(props) {
        super(props)

        this.state = {
            destinationName: '',
            destinationPrice: '',
            destinationDetail: '',
            destinationCity: '',
            destinationAdress: '',
            destinationRating: '',
            destinationCategory: '',
            // destinationImgBanner: ''
        };
        this.changeDestinationNameHandler = this.changeDestinationNameHandler.bind(this);
        this.changeDestinationPriceHandler = this.changeDestinationPriceHandler.bind(this);
        this.changeDestinationDetailHandler = this.changeDestinationDetailHandler.bind(this);
        this.changeDestinationCityHandler = this.changeDestinationCityHandler.bind(this);
        this.changeDestinationAdressHandler = this.changeDestinationAdressHandler.bind(this);
        this.changeDestinationRatingHandler = this.changeDestinationRatingHandler.bind(this);
        // this.changeDestinationImgBannerHandler = this.changeDestinationImgBannerHandler.bind(this);
        this.changeDestinationCategoryHandler = this.changeDestinationCategoryHandler.bind(this);

        this.saveDestination = this.saveDestination.bind(this);
    };


    changeDestinationNameHandler = (event) => {
        this.setState({ destinationName: event.target.value })
    }

    changeDestinationPriceHandler = (event) => {
        this.setState({ destinationPrice: event.target.value })
    }

    changeDestinationDetailHandler = (event) => {
        this.setState({ destinationDetail: event.target.value })
    }

    changeDestinationCityHandler = (event) => {
        this.setState({ destinationCity: event.target.value })
    }

    changeDestinationAdressHandler = (event) => {
        this.setState({ destinationAdress: event.target.value })
    }
    changeDestinationRatingHandler = (event) => {
        this.setState({ destinationRating: event.target.value })
    }
    changeDestinationCategoryHandler = (event) => {
        this.setState({ destinationCategory: event.target.value })
    }
    // changeDestinationImgBannerHandler = (event) => {
    //     this.setState({ destinationRating: event.target.value })
    // }

    saveDestination = (d) => {
        d.preventDefault();

        let destination = {
            destinationName: this.state.destinationName,
            destinationPrice: this.state.destinationPrice,
            destinationDetail: this.state.destinationDetail,
            destinationCity: this.state.destinationCity,
            destinationCategory: this.state.destinationCategory,
            // destinationImgBanner: this.state.destinationImgBanner

        };
        console.log('destination => ' + JSON.stringify(destination));

        DestinationServices.createDestination(destination).then(res => {
            this.props.history.push('/destinations');
        });

    }

    cancel() {
        this.props.history.push('/destinations');
    }

    render() {
        return (
            <div>
                <div className="container">
                    <div className="row">
                        <div className="card col-md-6 offset-md-3 offset-md-3">
                            <h3 className="text-center">Add Destination</h3>
                            <div className="card-body">

                                <form action="">
                                    <div className="form-group">
                                        <label htmlFor=""> Destination Name </label>
                                        <input name="destinationName" placeholder="Destination Name" className="form-control"
                                            value={this.state.destinationName} onChange={this.changeDestinationNameHandler} />
                                    </div>
                                    <div className="row">
                                        <div className="col">
                                            <div className="form-group">
                                                <label htmlFor=""> Destination Price </label>
                                                <input name="destinationPrice" placeholder="Destination Price" className="form-control"
                                                    value={this.state.destinationPrice} onChange={this.changeDestinationPriceHandler} />
                                            </div>
                                        </div>

                                        <div className="col">
                                            <div className="form-group">
                                                <label htmlFor=""> Destination City </label>
                                                <select name="destinationCity" class="form-control" onChange={this.changeDestinationCityHandler}>
                                                    <option value="Bandung">Bandung</option>
                                                    <option value="Jakarta">Jakarta</option>
                                                    <option value="Surabaya">Surabaya</option>
                                                    <option value="Yogyakarta">Yogyakarta</option>
                                                    <option value="Bali">Bali</option>
                                                    <option value="Surabaya">Surabaya</option>
                                                    <option value="Malang">Malang</option>
                                                </select>
                                            </div>
                                        </div>

                                    </div>
                                    <div className="form-group">
                                        <label htmlFor=""> Destination Detail </label>
                                        <textarea name="destinationDetail" placeholder="Destination Detail" className="form-control"
                                            value={this.state.destinationDetail} onChange={this.changeDestinationDetailHandler} />
                                    </div>

                                    <div className="row">
                                        <div className="col">
                                            <div className="form-group">
                                                <label htmlFor=""> Destination Address </label>
                                                <textarea name="destinationAdress" placeholder="Destination Address" className="form-control"
                                                    value={this.state.destinationAdress} onChange={this.changeDestinationAdressHandler} />
                                            </div>
                                        </div>

                                        <div className="col">
                                            <div className="form-group">
                                                <label htmlFor=""> Destination Rating </label>
                                                <input name="destinationRating" placeholder="Destination Rating" className="form-control"
                                                    value={this.state.destinationRating} onChange={this.changeDestinationRatingHandler} />
                                            </div>
                                        </div>
                                    </div>

                                    <div className="row">
                                        <div className="col">
                                            <div className="form-group">
                                                <label htmlFor=""> Destination Category </label>
                                                <select name="destinationCategory" class="form-control" onChange={this.changeDestinationCategoryHandler}>
                                                    <option disabled>Pilih Category</option>
                                                    <option value="1">Spa And Massage</option>
                                                    <option value="2">Atraksi</option>
                                                    <option value="3">Aktivitas</option>
                                                    <option value="4">Taman Hiburan</option>
                                                    <option value="5">Aktivitas Outdoor</option>
                                                    <option value="6">Aktivitas Air</option>
                                                </select>
                                            </div>
                                        </div>

                                        <div className="col">

                                        </div>

                                    </div>



                                    <div className="btn btn-success" onClick={this.saveDestination}>Save</div>
                                    <div className="btn btn-danger" onClick={this.cancel.bind(this)} style={{ marginLeft: "10 px" }} >Cancel</div>


                                </form>

                            </div>

                        </div>
                    </div>
                </div>
            </div>

        );
    }
}

export default CreateDestinationsComponent;
