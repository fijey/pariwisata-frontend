import React, { Component } from 'react';
import DestinationServices from '../services/DestinationServices';

export default class ListDestinationComponent extends Component {

    constructor(props) {
        super(props)

        this.state = {
            destinations: []

        };
        this.addDestination = this.addDestination.bind(this);
        this.editDestination = this.editDestination.bind(this);
        this.deleteDestination = this.deleteDestination.bind(this);
    };

    deleteDestination(id) {
        DestinationServices.deleteDestination(id).then(res => {
            this.setState({ destinations: this.state.destinations.filter(destination => destination.id !== id) });
        });
    }

    editDestination(id) {
        this.props.history.push(`/update-destinations/${id}`);
    }

    componentDidMount() {
        DestinationServices.getDestinations().then((res) => {
            this.setState({ destinations: res.data });
        });
    }

    addDestination() {
        this.props.history.push('add-destinations')
    }

    render() {
        return (
            <div>
                <h2 className="text-center"> Destination List</h2>
                <div className="row">
                    <button className="btn btn-primary" onClick={this.addDestination}> Add Destination</button>
                </div>
                <div className="row">
                    <table className="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th>Destination Name</th>
                                <th>Destination Price</th>
                                <th>Destination Detail</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            {
                                this.state.destinations.map(
                                    destination =>
                                        <tr key={destination.id}>
                                            <td>{destination.destinationName}</td>
                                            <td>{destination.destinationPrice}</td>
                                            <td>{destination.destinationDetail}</td>
                                            <td>
                                                <div className="btn-group" role="group">
                                                    <button onClick={() => this.editDestination(destination.id)} className="btn btn-info" > Update </button>
                                                    <button onClick={() => this.deleteDestination(destination.id)} className=" ml-3 btn btn-danger" > Delete </button>
                                                </div>
                                            </td>

                                        </tr>
                                )
                            }
                        </tbody>
                    </table>
                </div>

            </div>
        );
    }
}


