import React, { Component } from 'react';
import CategoryServices from '../services/CategoryServices';

export default class ListCategoryComponent extends Component {

    constructor(props) {
        super(props)

        this.state = {
            category: []

        };
        this.addCategory = this.addCategory.bind(this);
        this.editCategory = this.editCategory.bind(this);
        this.deleteCategory = this.deleteCategory.bind(this);
    };

    deleteCategory(id) {
        CategoryServices.deleteCategory(id).then(res => {
            this.setState({ categorys: this.state.categorys.filter(category => category.id !== id) });
        });
    }

    editCategory(id) {
        this.props.history.push(`/update-category/${id}`);
    }

    componentDidMount() {
        CategoryServices.getCategory().then((res) => {
            this.setState({ category: res.data });
        });
    }

    addCategory() {
        this.props.history.push('add-category')
    }

    render() {
        return (
            <div>
                <h2 className="text-center"> Category List</h2>
                <div className="row">
                    <button className="btn btn-primary" onClick={this.addCategory}> Add Category</button>
                </div>
                <div className="row">
                    <table className="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th>Category Name</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            {
                                this.state.category.map(
                                    category =>
                                        <tr key={category.id}>
                                            <td>{category.categoryName}</td>
                                            <td>
                                                <div className="btn-group" role="group">
                                                    <button onClick={() => this.editcategory(category.id)} className="btn btn-info" > Update </button>
                                                    <button onClick={() => this.deletecategory(category.id)} className=" ml-3 btn btn-danger" > Delete </button>
                                                </div>
                                            </td>

                                        </tr>
                                )
                            }
                        </tbody>
                    </table>
                </div>

            </div>
        );
    }
}


