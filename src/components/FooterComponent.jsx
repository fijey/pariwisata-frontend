import React, { Component } from 'react';

export default class FooterComponent extends Component {
    render() {
        return (
            <div>
                <footer className="footer navbar navbar-default navbar-static-bottom">
                    <span className="text-muted center"> Created By Fajar Mukti</span>
                </footer>
            </div>
        );
    }
}
