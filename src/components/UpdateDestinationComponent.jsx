import React, { Component } from 'react';
import CategoryServices from '../services/CategoryServices';
import DestinationServices from '../services/DestinationServices';

class UpdateDestinationsComponent extends Component {

    constructor(props) {
        super(props)

        this.state = {
            id: this.props.match.params.id,
            category: [],
            destinationName: '',
            destinationPrice: '',
            destinationDetail: '',
            destinationCity: '',
            destinationAdress: '',
            destinationRating: '',
            destinationCategory: '',
            // destinationImgBanner: ''
        };
        this.changeDestinationNameHandler = this.changeDestinationNameHandler.bind(this);
        this.changeDestinationPriceHandler = this.changeDestinationPriceHandler.bind(this);
        this.changeDestinationDetailHandler = this.changeDestinationDetailHandler.bind(this);
        this.changeDestinationCityHandler = this.changeDestinationCityHandler.bind(this);
        this.changeDestinationAdressHandler = this.changeDestinationAdressHandler.bind(this);
        this.changeDestinationRatingHandler = this.changeDestinationRatingHandler.bind(this);
        // this.changeDestinationImgBannerHandler = this.changeDestinationImgBannerHandler.bind(this);
        this.changeDestinationCategoryHandler = this.changeDestinationCategoryHandler.bind(this);

        this.UpdateDestination = this.UpdateDestination.bind(this);
    };

    componentDidMount() {
        DestinationServices.getDestinationById(this.state.id).then((res) => {
            let destination = res.data;
            console.log(destination);
            this.setState({
                destinationName: destination.destinationName,
                destinationPrice: destination.destinationPrice,
                destinationDetail: destination.destinationDetail,
                destinationCity: destination.destinationCity,
                // destinationImgBanner: destination.destinationImgBanner,
                destinationAdress: destination.destinationAdress,
                destinationRating: destination.destinationRating,
                destinationCategory: destination.destinationCategory
            })
        })

        CategoryServices.getCategory().then((res) => {
            this.setState({ category: res.data });
            console.log(res.data);
        });
    }

    changeDestinationNameHandler = (event) => {
        this.setState({ destinationName: event.target.value })
    }

    changeDestinationPriceHandler = (event) => {
        this.setState({ destinationPrice: event.target.value })
    }

    changeDestinationDetailHandler = (event) => {
        this.setState({ destinationDetail: event.target.value })
    }

    changeDestinationCityHandler = (event) => {
        this.setState({ destinationCity: event.target.value })
    }

    changeDestinationAdressHandler = (event) => {
        this.setState({ destinationAdress: event.target.value })
    }
    changeDestinationRatingHandler = (event) => {
        this.setState({ destinationRating: event.target.value })
    }
    changeDestinationCategoryHandler = (event) => {
        this.setState({ destinationCategory: event.target.value })
    }
    // changeDestinationImgBannerHandler = (event) => {
    //     this.setState({ destinationRating: event.target.value })
    // }

    UpdateDestination = (d) => {
        d.preventDefault();

        let destination = {
            destinationName: this.state.destinationName,
            destinationPrice: this.state.destinationPrice,
            destinationDetail: this.state.destinationDetail,
            destinationCity: this.state.destinationCity,
            // destinationImgBanner: this.state.destinationImgBanner,
            destinationAdress: this.state.destinationAdress,
            destinationRating: this.state.destinationRating,
            destinationCategory: this.state.destinationCategory
        };
        console.log('destination => ' + JSON.stringify(destination));

        DestinationServices.updateDestination(this.state.id, destination)
            .then(res => {
                this.props.history.push('/destinations');
            })

    }

    cancel() {
        this.props.history.push('/destinations');
    }

    render() {
        return (
            <div>
                <div className="container">
                    <div className="row">
                        <div className="card col-md-6 offset-md-3 offset-md-3">
                            <h3 className="text-center">Update Destination</h3>
                            <div className="card-body">

                                <form action="">
                                    <div className="form-group">
                                        <label htmlFor=""> Destination Name </label>
                                        <input name="destinationName" placeholder="Destination Name" className="form-control"
                                            value={this.state.destinationName} onChange={this.changeDestinationNameHandler} />
                                    </div>
                                    <div className="row">
                                        <div className="col">
                                            <div className="form-group">
                                                <label htmlFor=""> Destination Price </label>
                                                <input name="destinationPrice" placeholder="Destination Price" className="form-control"
                                                    value={this.state.destinationPrice} onChange={this.changeDestinationPriceHandler} />
                                            </div>
                                        </div>

                                        <div className="col">
                                            <div className="form-group">
                                                <label htmlFor=""> Destination City </label>
                                                <select value={this.state.destinationCity} name="destinationCity" className="form-control" onChange={this.changeDestinationCityHandler}>
                                                    <option value="Bandung">Bandung</option>
                                                    <option value="Jakarta">Jakarta</option>
                                                    <option value="Surabaya">Surabaya</option>
                                                    <option value="Yogyakarta">Yogyakarta</option>
                                                    <option value="Bali">Bali</option>
                                                    <option value="Surabaya">Surabaya</option>
                                                    <option value="Malang">Malang</option>
                                                </select>
                                            </div>
                                        </div>

                                    </div>
                                    <div className="form-group">
                                        <label htmlFor=""> Destination Detail </label>
                                        <textarea name="destinationDetail" placeholder="Destination Detail" className="form-control"
                                            value={this.state.destinationDetail} onChange={this.changeDestinationDetailHandler} />
                                    </div>

                                    <div className="row">
                                        <div className="col">
                                            <div className="form-group">
                                                <label htmlFor=""> Destination Address </label>
                                                <textarea name="destinationAdress" placeholder="Destination Address" className="form-control"
                                                    value={this.state.destinationAdress} onChange={this.changeDestinationAdressHandler} />
                                            </div>
                                        </div>

                                        <div className="col">
                                            <div className="form-group">
                                                <label htmlFor=""> Destination Rating </label>
                                                <input name="destinationRating" placeholder="Destination Rating" className="form-control"
                                                    value={this.state.destinationRating} onChange={this.changeDestinationRatingHandler} />
                                            </div>
                                        </div>
                                    </div>

                                    <div className="row">
                                        <div className="col">
                                            <div className="form-group">
                                                <label htmlFor=""> Destination Category </label>
                                                <select value={this.state.destinationCategory} name="destinationCategory" className="form-control" onChange={this.changeDestinationCategoryHandler}>
                                                    <option value="" disabled selected>Pilih Category ...</option>
                                                    {this.state.category.map(
                                                        (category) =>
                                                            <option key={category.categoryId} value={category.categoryId} >{category.categoryName}</option>
                                                    )}
                                                </select>
                                            </div>
                                        </div>

                                        <div className="col">

                                        </div>

                                    </div>

                                    <div className="btn btn-success" onClick={this.UpdateDestination}>Save</div>
                                    <div className="btn btn-danger" onClick={this.cancel.bind(this)} style={{ marginLeft: "10 px" }} >Cancel</div>


                                </form>

                            </div>

                        </div>
                    </div>
                </div>
            </div>

        );
    }
}

export default UpdateDestinationsComponent;
