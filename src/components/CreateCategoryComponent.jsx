import React, { Component } from 'react';
import CategoryServices from '../services/CategoryServices';

class CreateCategoryComponent extends Component {

    constructor(props) {
        super(props)

        this.state = {
            categoryName: ''
        };
        this.changeCategoryNameHandler = this.changeCategoryNameHandler.bind(this);
        this.saveCategory = this.saveCategory.bind(this);
    };


    changeCategoryNameHandler = (event) => {
        this.setState({ categoryName: event.target.value })
    }

    saveCategory = (d) => {
        d.preventDefault();

        let category = {
            categoryName: this.state.categoryName

        };
        console.log('category => ' + JSON.stringify(category));

        CategoryServices.createCategory(category).then(res => {
            this.props.history.push('/category');
        });

    }

    cancel() {
        this.props.history.push('/category');
    }

    render() {
        return (
            <div>
                <div className="container">
                    <div className="row">
                        <div className="card col-md-6 offset-md-3 offset-md-3">
                            <h3 className="text-center">Add Category</h3>
                            <div className="card-body">

                                <form action="">
                                    <div className="form-group">
                                        <label htmlFor=""> Category Name </label>
                                        <input name="categoryName" placeholder="Category Name" className="form-control"
                                            value={this.state.categoryName} onChange={this.changeCategoryNameHandler} />
                                    </div>

                                    <div className="btn btn-success" onClick={this.saveCategory}>Save</div>
                                    <div className="btn btn-danger" onClick={this.cancel.bind(this)} style={{ marginLeft: "10 px" }} >Cancel</div>


                                </form>

                            </div>

                        </div>
                    </div>
                </div>
            </div>

        );
    }
}

export default CreateCategoryComponent;
