import React from 'react';
import logo from './logo.svg';
import './App.css';
import ListDestinationComponent from './components/ListDestinationComponent';
import ListCategoryComponent from './components/ListCategoryComponent';
import CreateCategoryComponent from './components/CreateCategoryComponent';
import HeaderComponent from './components/HeaderComponent';
import FooterComponent from './components/FooterComponent';

import {BrowserRouter as Router, Route, Switch} from 'react-router-dom';
import CreateDestinationsComponent from './components/CreateDestinationsComponent';
import UpdateDestinationComponent from './components/UpdateDestinationComponent';

function App() {
  return (
    <div>
      <Router>
            <HeaderComponent/>
             <div className="container">
               <Switch>
                    <Route path ="/" exact component= {ListDestinationComponent}></Route>
                    <Route path ="/destinations" component= {ListDestinationComponent}></Route>
                    <Route path ="/add-destinations" component= {CreateDestinationsComponent}></Route>
                    <Route path ="/update-destinations/:id" component= {UpdateDestinationComponent}></Route>

                    <Route path ="/add-category" component= {CreateCategoryComponent}></Route>
                    <Route path ="/category" component= {ListCategoryComponent}></Route>

               </Switch>
             </div> 
             <FooterComponent/>
      </Router>
    </div>
  );
}

export default App;
