import axios from 'axios';

const DESTINATION_BASE_URL = "http://localhost:8090/api/v1/destination";
class DestinationServices {

    getDestinations(){
        return axios.get(DESTINATION_BASE_URL);
    }

    createDestination(destination) {
        return axios.post(DESTINATION_BASE_URL,destination)
    }

    getDestinationById(destinationId) {
        return axios.get(DESTINATION_BASE_URL+'/'+destinationId);
    }
    updateDestination(destinationId, destination){
        return axios.put(DESTINATION_BASE_URL+'/'+destinationId, destination);
    }

    deleteDestination(destinationId){
        return axios.delete(DESTINATION_BASE_URL+'/'+destinationId);
    }

}

export default new DestinationServices;