import axios from 'axios';

const CATEGORY_BASE_URL = "http://localhost:8090/api/v1/category";
class CategoryServices {

    getCategory(){
        return axios.get(CATEGORY_BASE_URL);
    }

    createCategory(category) {
        return axios.post(CATEGORY_BASE_URL,category)
    }

    // getDestinationById(destinationId) {
    //     return axios.get(CATEGORY_BASE_URL+'/'+destinationId);
    // }
    // updateDestination(destinationId, destination){
    //     return axios.put(CATEGORY_BASE_URL+'/'+destinationId, destination);
    // }

    // deleteDestination(destinationId){
    //     return axios.delete(CATEGORY_BASE_URL+'/'+destinationId);
    // }

}

export default new CategoryServices;